# contact printer

A desktop 16mm contact printer made for CNC/laser cutting and 3D printing

---

## [Project Home: git.sixteenmillimeter.com/16mm/contact_printer](https://git.sixteenmillimeter.com/16mm/contact_printer)

### Mirrors

* [github.com/sixteenmillimeter/contact_printer](https://github.com/sixteenmillimeter/contact_printer)
* [gitlab.com/16mm/contact_printer](https://gitlab.com/16mm/contact_printer)

---

![contact printer render](https://github.com/sixteenmillimeter/contact_printer/blob/master/img/contact_printer?raw=true)
![contact printer render - bottom](https://github.com/sixteenmillimeter/contact_printer/blob/master/img/contact_printer_2?raw=true)
